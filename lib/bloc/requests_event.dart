part of 'requests_bloc.dart';

// @immutable
// abstract class RequestsEvent {}

abstract class RequestsEvent extends Equatable {
  const RequestsEvent();
  @override
  List<Object> get props => [];
}

class FirstEvent extends RequestsEvent {
  final String zone;
  FirstEvent({required this.zone});

  Object? get getZone => zone;
}

class ChangeEvent extends RequestsEvent {
  ChangeEvent();
}
// TODO: change this
