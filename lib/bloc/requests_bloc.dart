import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math';
part 'requests_event.dart';
part 'requests_state.dart';

class RequestsBloc extends Bloc<RequestsEvent, RequestsState> {
  RequestsBloc() : super(RequestsInitial()) {
    on<FirstEvent>((event, emit) async {
      try {
        var zone = event.getZone;
        var urlPhrase = Uri.parse('https://zenquotes.io/api/random');
        var responsePhrase = await http.get(urlPhrase);
        List<dynamic> mapPhrase = json.decode(responsePhrase.body);

        var urlTime = Uri.parse('http://worldtimeapi.org/api/timezone/${zone}');
        var responseTime = await http.get(urlTime);
        Map<String, dynamic> mapTime = json.decode(responseTime.body);

        var time = mapTime["datetime"].substring(
            mapTime["datetime"].indexOf("T") + 1,
            mapTime["datetime"].indexOf("."));

        var rng = Random();
        var width = 300 + rng.nextInt(5);

        var urlPhoto = "https://picsum.photos/${width}/400";

        emit(RequestsLoadedState(
            mapPhrase: mapPhrase, date: time, urlPhoto: urlPhoto));
      } catch (err) {
        print(err);
        emit(RequestsErrorState(errorMsg: err.toString()));
      }
    });
    on<ChangeEvent>((event, emit) async {
      emit(RequestsLoadingState());
    });
  }
}
