part of 'requests_bloc.dart';

abstract class RequestsState extends Equatable {
  const RequestsState();
  @override
  List<Object> get props => [];
}

class RequestsInitial extends RequestsState {}

class RequestsLoadingState extends RequestsState {}

class RequestsLoadedState extends RequestsState {
  final List<dynamic> mapPhrase;
  final String date;
  final String urlPhoto;
  RequestsLoadedState(
      {required this.mapPhrase, required this.date, required this.urlPhoto});

  @override
  List<Object> get props => [];
}

class RequestsErrorState extends RequestsState {
  final String errorMsg;

  RequestsErrorState({required this.errorMsg});

  @override
  List<Object> get props => [errorMsg];
}
