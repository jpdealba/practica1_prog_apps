import 'package:flutter/material.dart';
import 'package:money_track/bloc/requests_bloc.dart';
import 'package:money_track/home.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.brown),
        home: MultiBlocProvider(
          providers: [
            BlocProvider<RequestsBloc>(
              create: (BuildContext context) => RequestsBloc(),
            )
          ],
          child: HomePage(),
        ));
  }
}
