import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:backdrop/backdrop.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/requests_bloc.dart';

var list = [
  ['Andorra', 'Europe/Andorra', 'https://flagcdn.com/w40/ad.png'],
  ['Mexico', 'America/Mexico_City', 'https://flagcdn.com/w40/mx.png'],
  ['Peru', 'America/Lima', 'https://flagcdn.com/w40/pe.png'],
  ['Canada', 'America/Vancouver', 'https://flagcdn.com/w40/ca.png'],
  ['Argentina', 'America/Argentina/Salta', 'https://flagcdn.com/w40/ar.png'],
];

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void initState() {
    BlocProvider.of<RequestsBloc>(context)
        .add(FirstEvent(zone: 'America/Mexico_City'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BackdropScaffold(
      headerHeight: 500.0,
      appBar: BackdropAppBar(
        title: Text("La frase diaria"),
        actions: [
          BackdropToggleButton(
            icon: AnimatedIcons.list_view,
          )
        ],
      ),
      backLayer: Padding(
        padding: const EdgeInsets.only(bottom: 500.0),
        child: ListView.separated(
          separatorBuilder: (BuildContext context, int index) {
            return Divider();
          },
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return RawMaterialButton(
              onPressed: () {
                BlocProvider.of<RequestsBloc>(context).add(ChangeEvent());
                BlocProvider.of<RequestsBloc>(context)
                    .add(FirstEvent(zone: list[index][1]));
              },
              child: Row(children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    children: [
                      Image.network(list[index][2]),
                      Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: Text(
                          "${list[index][0]}",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      )
                    ],
                  ),
                )
              ]),
            );
          },
        ),
      ),
      frontLayer: Center(
          child: BlocConsumer<RequestsBloc, RequestsState>(
              builder: (context, state) {
        if (state is RequestsLoadedState) {
          var phrase = state.mapPhrase[0]["q"];
          var by = state.mapPhrase[0]["a"];

          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.8), BlendMode.darken),
                image: NetworkImage("${state.urlPhoto}"),
                fit: BoxFit.cover,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 300.0),
                        child: Text(
                          "${state.date}",
                          style: TextStyle(color: Colors.white, fontSize: 50),
                        ),
                      ),
                    ),
                    Text("$phrase",
                        style: TextStyle(color: Colors.white, fontSize: 17)),
                    Text("- $by",
                        style: TextStyle(color: Colors.white, fontSize: 17))
                  ]),
            ),
          );
        } else {
          return CircularProgressIndicator();
        }
      }, listener: (context, state) {
        if (state is RequestsErrorState) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("${state.errorMsg}"),
            ),
          );
        }
      })),
    );
  }
}
